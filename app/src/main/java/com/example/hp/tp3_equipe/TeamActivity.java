package  com.example.hp.tp3_equipe;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

//import fr.uavignon.shuet.tp3.data.Match;
//import fr.uavignon.shuet.tp3.data.Team;
//import fr.uavignon.shuet.tp3.webservice.WebServiceGet;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;


    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private ImageView imageBadge;
    private Team team;

    String url;
    FetchData data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);
        // match between the two activivites
        team = (Team) getIntent().getParcelableExtra("teamActv");
        Log.i("call to teamActivity", " done?");


        team = (Team) getIntent().getParcelableExtra(Team.TAG);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.league);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        imageBadge = (ImageView) findViewById(R.id.imageView);


        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (data != null)
                    //data.cancel(true);
                    data = new FetchData();
                    data.execute();
            }
        });

    }

    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity

        super.onBackPressed();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        //TODO : update imageBadge
       /* if (team.getTeamBadge()!=null && !team.getTeamBadge().isEmpty()) {
            Log.d(TAG,"icon="+"icon_" + team.getTeamBadge());
            imageBadge.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/"+"icon_" + team.getTeamBadge(), null, getPackageName())));

        }*/

        url = (team.getTeamBadge());
        Log.i("imageBradge URL ", " --> "+team.getTeamBadge());
        loadImageView(url);

    }

    public static String loadTeamContent(Team team, JSONResponseHandlerTeam responseHandlerTeam, Context context) {
        HttpsURLConnection urlConnection = null;
        ConnectivityManager connect = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connect.getActiveNetworkInfo();

        if (activeNetwork != null) {

            URL url = null;
            try {
                url = WebServiceUrl.buildSearchTeam(team.getName());
                urlConnection = (HttpsURLConnection) url.openConnection();

                Log.i("upload", "Téléchargement terminé, code http : " + urlConnection);

                if (urlConnection.getResponseCode() == 200) {
                    InputStream response = urlConnection.getInputStream();
                    // read the data from the stream
                    responseHandlerTeam.readJsonStream(response);
                }else{
                    Toast.makeText(context, "erreur de connexion au serveur", Toast.LENGTH_SHORT).show();

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
        return "executed";

    }


    public static String loadTeamLastEvent(Team team, JSONResponseHandlerTeam responseHandlerTeam, Context context) {

        HttpsURLConnection urlConnection = null;
        ConnectivityManager connect = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connect.getActiveNetworkInfo();

        try {
            if (activeNetwork != null) {


                URL url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                urlConnection = (HttpsURLConnection) url.openConnection();

                Log.i("upload", "Téléchargement terminé, code http : " + urlConnection);

                if (urlConnection.getResponseCode() == 200) {
                    InputStream response = urlConnection.getInputStream();
                    responseHandlerTeam.readJsonStream(response);
                } else {
                    Toast.makeText(context, "erreur de connexion au serveur", Toast.LENGTH_SHORT).show();

                }
            }else {
                Toast.makeText(context, "  Veuillez vous connectez à internet", Toast.LENGTH_SHORT).show();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            return "executed";
        }

    }


    public static String loadTeamRank(Team team, JSONResponseHandlerTeam responseHandlerTeam, Context context) {

        HttpsURLConnection urlConnection = null;
        ConnectivityManager connect = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connect.getActiveNetworkInfo();

        try {
            if (activeNetwork != null) {


                URL url = WebServiceUrl.buildGetRanking(team.getIdLeague());
                urlConnection = (HttpsURLConnection) url.openConnection();

                Log.i("upload", "Téléchargement terminé, code http : " + urlConnection);

                if (urlConnection.getResponseCode() == 200) {
                    InputStream response = urlConnection.getInputStream();
                    responseHandlerTeam.readJsonStream(response);
                }else {
                    Toast.makeText(context, "erreur de connexion au serveur", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(context, "  Veuillez vous connectez à internet", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            return "executed";
        }

    }

    public class FetchData extends AsyncTask<Void,Void,Void> {

        private Team team;
        private JSONResponseHandlerTeam responseHandlerTeam;

        // background task
        // recuperer l HttpURLConnection a partir de URL definie dans la classe WebServiceURL
        // Analyse syntaxique de la réponse en JSON du bew serveice a l'aide de la classe JSONResponseHandlerTeam
        @Override
        protected Void doInBackground(Void... voids) {
            String task1 = loadTeamContent(team, responseHandlerTeam, TeamActivity.this);
            String task2 = loadTeamLastEvent(team, responseHandlerTeam, TeamActivity.this);

            return null;
        }


        //mettre a jour la vue
        @Override
        protected void onPostExecute(Void aVoid) {
              updateView();
        }
    }


    private void loadImageView(String url) {
        Picasso.with(this).load(url).placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(imageBadge, new com.squareup.picasso.Callback() {

                    @Override
                    public void onSuccess() {
                        Toast.makeText(getApplicationContext(), "Fetched image from internet", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError() {
                        Toast.makeText(getApplicationContext(), "An error occurred", Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
