package com.example.hp.tp3_equipe;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {
    SportDbHelper dbHelper;
    Cursor result;
    Intent myIntent;
     ListView listV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        dbHelper = new SportDbHelper(this);
        result=dbHelper.fetchAllTeams();

        dbHelper.populate();
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllTeams() ,
                new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.COLUMN_LEAGUE_NAME},
                new int[] { android.R.id.text1, android.R.id.text2},0);
         listV  = (ListView) findViewById(R.id.listEquipe);
        listV.setAdapter(cursorAdapter);
        System.out.println("teste list ?");
        listV.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
             // ouvrir TeamActivity ici
                result.moveToPosition(position);
                Team team = dbHelper.cursorToTeam(result);
                // test empty statement
                if (team == null) {
                    Log.d("~", "team est null");
                }else{
                    Log.d("~", "team n'est pas null");
                }
                myIntent = new Intent(MainActivity.this, TeamActivity.class).putExtra("teamActv", team);
                MainActivity.this.startActivity(myIntent);
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Team t = new Team(" "," ");
                Intent myIntent = new Intent(MainActivity.this, NewTeamActivity.class);
                myIntent.putExtra("equipe", t);
                MainActivity.this.startActivity(myIntent);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();
        String action = String.valueOf(item.getTitle());
        if(action.equals("Supprimer")){
            long position = info.id;
            result.moveToPosition((int) position-1);
            Team t = dbHelper.cursorToTeam(result);
            if(dbHelper.deleteTeam(t.getId())){
                Snackbar.make(info.targetView, t.getId()+" Supprimé !", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Log.i("supression","effectuer");
                // refresh
                dbHelper = new SportDbHelper(this);
                Cursor result = dbHelper.fetchAllTeams();
                SimpleCursorAdapter adapter = new SimpleCursorAdapter (this,
                        android.R.layout.simple_list_item_2,
                        dbHelper.fetchAllTeams() ,
                        new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.COLUMN_LEAGUE_NAME},
                        new int[] { android.R.id.text1, android.R.id.text2},0);
                listV.setAdapter(adapter);
            }
            else{
                Snackbar.make(info.targetView, "Suppression echoué", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Log.i("supression","non effectuer");
            }

        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllTeams() ,
                new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.COLUMN_LEAGUE_NAME},
                new int[] { android.R.id.text1, android.R.id.text2},0);
        final ListView listView = (ListView) findViewById(R.id.listEquipe);
        listView.setAdapter(cursorAdapter);
    }

}
